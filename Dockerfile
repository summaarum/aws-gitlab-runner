# use apache docker image
# https://hub.docker.com/_/httpd
FROM httpd

# copy files into docker image htdocs root
COPY ./web/ /usr/local/apache2/htdocs/


# you can test this docker image
#
# build docker image
# docker build -t test-web1 .
# run docker image
# docker run --name=test-web1 --rm -p "80:80" test-web1
#
# open http://localhost/ in your browser
# or if docker is not in localhost - use docker-machine ip value instead of localhost